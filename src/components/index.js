/* eslint-disable import/prefer-default-export */
import GlComment from '~/components/comment.vue';
import GlHead from '~/components/header.vue';
import GlListIncident from '~/components/list_incident.vue';
import GlMarkdown from '~/components/markdown.vue';

export { GlListIncident, GlHead, GlMarkdown, GlComment };

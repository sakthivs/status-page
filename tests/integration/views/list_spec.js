/* eslint-disable promise/always-return */
import { GlAlert, GlLoadingIcon, GlBadge, GlFormInput, GlIcon } from '@gitlab/ui';
import { shallowMount, RouterLinkStub } from '@vue/test-utils';
import axios from 'axios';
import { GlListIncident } from '~/components';
import List from '~/views/list.vue';

jest.mock('axios');
axios.get.mockResolvedValue({
  data: {
    incidents: [
      {
        id: 1,
        title: 'Title',
        description: 'incident',
        status: 'opened',
        created_at: '2020-02-21T07:05:38.261Z',
        links: { details: 'data/incident/1.json' },
      },
      {
        id: 2,
        title: 'Title',
        description: 'incident2',
        status: 'opened',
        created_at: '2020-02-21T07:05:38.261Z',
        links: { details: 'data/incident/1.json' },
      },
    ],
  },
});

describe('List page', () => {
  let wrapper;

  function mountComponent() {
    wrapper = shallowMount(List, {
      stubs: { GlLoadingIcon, GlAlert, GlBadge, GlFormInput, GlIcon, RouterLink: RouterLinkStub },
    });
  }

  beforeEach(() => {
    mountComponent();
  });

  afterEach(() => {
    if (wrapper) {
      wrapper.destroy();
    }
  });

  const findAlert = () => wrapper.find(GlAlert);
  const findLoader = () => wrapper.find(GlLoadingIcon);
  const findIncidents = () => wrapper.findAll(GlListIncident);

  it('renders the incident list when data received', () => {
    expect(wrapper.element).toMatchSnapshot();
    expect(findLoader().exists()).toBe(false);
    expect(findIncidents().length).toBe(2);
  });

  it('renders success alert with no incidents message when there are no incidents', () => {
    wrapper.setData({ incidents: [], loading: false });
    expect(findLoader().exists()).toBe(false);

    return wrapper.vm.$nextTick().then(() => {
      expect(findAlert().exists()).toBe(true);
      expect(findAlert().props('variant')).toBe('success');
      expect(findAlert().text()).toBe(
        'There are no active incidents to report. All systems are operational.',
      );
    });
  });

  it('renders warning alert when an error is received', () => {
    wrapper.setData({ incidents: null, loading: false, error: true });
    expect(findLoader().exists()).toBe(false);

    return wrapper.vm.$nextTick().then(() => {
      expect(findAlert().exists()).toBe(true);
      expect(findAlert().props('variant')).toBe('warning');
      expect(findAlert().text()).toBe(
        'There was an error fetching the incidents. Please refresh the page to try again.',
      );
    });
  });
});
